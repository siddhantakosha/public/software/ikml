from ikml_doc.utils import *
import json,re
import sys
from anytree import RenderTree
from pathlib import Path
def combine_nums_last(vakyas):
    updated_vakyas=[]
    view_completed=[]
    if len(vakyas)>2:
        for index in range(len(vakyas)-1):
            if index in view_completed:
                continue
            if index==len(vakyas)-2:
                break
            next_index=index+1
            while re.match(re.compile(r"^\d"),vakyas[next_index]):
                view_completed.append(next_index)
                vakyas[index]+=vakyas[next_index]
                next_index+=1
                if next_index==len(vakyas)-1:
                    break
                #print(index,"==>",next_index)
            updated_vakyas.append(vakyas[index])
        del vakyas
    return updated_vakyas

def combine_numerics_with_previous(vakyas):
    len_vakyas=len(vakyas)
    deleting_index=[]
    if len_vakyas>2 :
        for i in range(1,len_vakyas-1):
            if vakyas[i].replace(" ","")=="॥" or vakyas[i].replace(" ","")=="।" or vakyas[i].replace(" ","")==".":
                vakyas[i-1]+=vakyas[i]
                deleting_index.append(i)
    for i in sorted(deleting_index,reverse=True):
        del vakyas[i]
    vakyas=combine_nums_last(vakyas)
    return vakyas
def vision_json_to_ikml(myfile):
    vakyas=[]
    root = Node("[root]")
    pages_number=0
    with open(myfile, "r", encoding="utf-8") as file:
        data = json.load(file)
    for each_page in data["fullTextAnnotation"]["pages"]:
        page = Node("[page]",  parent = root)
        page_width = each_page["width"]
        page_height = each_page["height"]
        Node(f"[.width] {page_width}", parent = page)
        Node(f"[.height] {page_height}", parent = page)
        for each_block in each_page["blocks"]:
            block = Node("[block]", parent = page)
            bbox = []
            for vtx in each_block["boundingBox"]["normalizedVertices"]:
                bbox.append(str(vtx["x"]) + ", " + str(vtx["y"]))
            Node("[.bbox] {}".format("; ".join(bbox)), parent = block)
            for each_paragraph in each_block["paragraphs"]:
                paragraph = Node("[paragraph]", parent = block)
                bbox = []
                for vtx in each_paragraph["boundingBox"]["normalizedVertices"]:
                    bbox.append(str(vtx["x"]) + ", " + str(vtx["y"]))
                Node("[.bbox] {}".format("; ".join(bbox)), parent = paragraph)
                buf_vakyas = []
                for each_word in each_paragraph["words"]:
                    buf_vakyas.append(" ")
                    for each_symbol in each_word["symbols"]:
                        buf_vakyas.append(each_symbol["text"])
                    vakyas = re.split(r"(॥|।|\.)", "".join(buf_vakyas).replace("||", "॥").replace("|", "।").strip())
                vakyas=combine_numerics_with_previous(vakyas)
                for vakya in vakyas:
                    if not vakya:
                        continue
                    va="[va] {}".format(vakya)
                    vak = Node(va, parent = paragraph)
    eachpage_ikml="[page]"
    all_pages_ikml=[]
    full_file_ikml=Node.tree_as_list(root)
    for item in full_file_ikml[1:]:
        if item!="[page]":
            eachpage_ikml+="\n"+item
        else:
            all_pages_ikml.append(eachpage_ikml)
            eachpage_ikml="[page]"
    all_pages_ikml.append(eachpage_ikml)
    for each_page_ikml in all_pages_ikml:
        fin=str(all_pages_ikml.index(each_page_ikml)+1)
        filename=f"{str(myfile).replace('.json','')}-page-{fin.zfill(3)}-ikml.txt"
        with open(filename,"w+",encoding="utf-8") as fl:
            fl.write(each_page_ikml)
        
if __name__=="__main__":
    myfile=sys.argv[1]
    vision_json_to_ikml(myfile)