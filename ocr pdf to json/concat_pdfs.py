#!/usr/bin/env python3

from glob import glob
import PyPDF2

# step into a folder

folders = glob("*")

for fd in folders:
    pdfs = sorted(glob(f"{fd}/*.pdf"))
    pdfMerge = PyPDF2.PdfMerger()
    for filename in pdfs:
        pdfFile = open(filename, 'rb')
        pdfReader = PyPDF2.PdfReader(pdfFile)
        pdfMerge.append(pdfReader)
        pdfFile.close()
    pdfMerge.write(f"../MERGED_PDFs/{fd}.pdf")