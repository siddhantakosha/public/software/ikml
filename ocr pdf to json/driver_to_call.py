from ocr_json_to_ikml import *
from visionpdf_to_json_multiple import *
from pathlib import Path
import sys,time,os,shutil
from multiprocessing import Pool

def file_extraction(folder_path,all_pdf_files):
    for i in Path(folder_path).iterdir():
        if Path(i).is_dir():
            all_pdf_files=file_extraction(i,all_pdf_files)
        else:
            if Path(i).suffix==".pdf":
                all_pdf_files.append(i)
            else:
              print(f"{i} is not a pdf file! Program accepets only pdf file inputs")
    return all_pdf_files

def main(pdf_input_file,no_of_papers_wanted_to_process=10):
    current_working_directory=Path(os.getcwd())
    common_parts=[]
    for part1, part2 in zip(current_working_directory.parts, pdf_input_file.parts):
        if part1 == part2:
            common_parts.append(part1)
        else:
            break
    common_path = Path(*common_parts)
    hirarchy_path = (pdf_input_file.with_suffix("")).relative_to(common_path)
    only_ikml_files="IKML_FILES"
    output_folder="OUTPUTFOLDER_DETAILED_CONTENTS"
    Respective_folder=output_folder/hirarchy_path
    Respective_folder.mkdir(parents=True,exist_ok=True)
    shutil.copy2(pdf_input_file,Respective_folder)
    file_input_program_takes=Respective_folder/pdf_input_file.name
    error_files="ERROR_CONTAINING_FILES"
    try:
        if not (Path(only_ikml_files).exists() and Path(output_folder/hirarchy_path/f"{pdf_input_file.stem}-ikml.txt").exists()):
            #SPECIFY THE PAGES YOU WANT TO GET IKML's FOR IN THE VARIABLE "no_of_papers_wanted_to_process=NO OF PAGES YOU WANT" 
            pdf_to_json(file_input_program_takes,Respective_folder,no_of_papers_wanted_to_process)
            for json_file in Path(Respective_folder).iterdir():
                if (json_file.name) == pdf_input_file.stem+".json":
                    vision_json_to_ikml(json_file)
            IKML_FILE=output_folder/hirarchy_path/f"{pdf_input_file.stem}-ikml.txt"
            COPY_OF_IKML_FILES=pdf_input_file.cwd()/only_ikml_files
            Path(COPY_OF_IKML_FILES).mkdir(exist_ok=True,parents=True)
            shutil.copy2(IKML_FILE,COPY_OF_IKML_FILES)
        else:
            #print(f"{Path(file_input_program_takes).name}'s IKML is already generated")
            pass
    except Exception as e:
        with open(error_files,"a+") as error_files:
            error_files.write(f"{file_input_program_takes} ==> {e}\n")

if __name__=="__main__":
    start=time.time()
    current_working_directory=os.getcwd()
    no_of_cpu=os.cpu_count()
    all_pdf_files=[]
    for fpath in sys.argv[1:]:
        if Path(fpath).is_dir():
            all_pdf_files=file_extraction(fpath,all_pdf_files)
        elif Path(fpath).suffix==".pdf":
            all_pdf_files.append(Path(fpath))
        else:
            print(f"{fpath} is other than a pdf file. The Program will accept only pdf files")
    with Pool(no_of_cpu) as pool:
        pool.map(main,all_pdf_files)
    """for i in all_pdf_files:
        main(i)"""
    #print(f"Total No.of padf's are : {len(all_pdf_files)}")
    end=time.time()
    print("The time taken by the Program is ",end-start)