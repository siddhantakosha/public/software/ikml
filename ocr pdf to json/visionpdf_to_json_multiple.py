import sys,re,os
from google.cloud import vision_v1
import json,proto,PyPDF2
from pathlib import Path
from PyPDF2 import PdfReader,PdfWriter
from ocr_json_to_ikml import *
service_key="credsjson.json"
servicekey_path=os.path.join(os.getcwd(),service_key)
os.environ["GOOGLE_APPLICATION_CREDENTIALS"]=servicekey_path


def single_response_json_generation(each_json_file,all_pages_responses):
    with open(each_json_file,"r",encoding="utf-8") as fr:
        each_json=json.load(fr)
    response1=each_json["responses"]
    for responses_ in response1:
        for fulltextann in responses_["responses"]:
            try:
                for each_page_text in fulltextann["fullTextAnnotation"]["pages"]:
                    all_pages_responses["fullTextAnnotation"]["pages"].append(each_page_text)
            except KeyError:
                pass
    filename=each_json_file
    each_json_file.unlink()
    filename=re.sub(r"-pdf[^.]*\.json$",".json",str(filename))
    with open(filename,"w",encoding="utf-8") as file:
        json.dump(all_pages_responses,file,indent=4)
    return all_pages_responses

def pdf_to_vision_response(pdf_file,Respective_Folder):
    #print("pdf_to_vision_response")
    with open(pdf_file,"rb") as f:
        content = f.read()
        client = vision_v1.ImageAnnotatorClient()
        mime_type = "application/pdf"
        features = [{"type_": vision_v1.Feature.Type.DOCUMENT_TEXT_DETECTION}]
        input_config = {"mime_type": mime_type, "content": content}
        requests = [{"input_config": input_config, "features": features, "pages": [1,2,3,4,5]}]
        response = client.batch_annotate_files(requests=requests)
        response=proto.Message.to_json(response)
        response=json.loads(response)
        filename=pdf_file.stem+".json"
        json_responses=Respective_Folder/filename
        with open(json_responses,"w",encoding="utf-8") as fw:
            json.dump(response,fw,indent=4)
    pdf_file.unlink()

def pdf_splitting(input_pdf_file,Respective_Folder,no_of_papers_wanted_to_process=None):
    base_name=input_pdf_file.stem
    pdf_list=[]
    pages_per_pdf=5
    with open(input_pdf_file,"rb") as file:
        pdf_reader=PyPDF2.PdfReader(file)
        pdf_reader=PdfReader(file)
        pdf_writer=PdfWriter()
        total_pages=len(pdf_reader.pages) if no_of_papers_wanted_to_process==None else (no_of_papers_wanted_to_process if no_of_papers_wanted_to_process<len(pdf_reader.pages) else len(pdf_reader.pages))
        pdfs_required=total_pages//pages_per_pdf
        if (total_pages%pages_per_pdf)!=0:
            pdfs_required+=1
        elif(total_pages==pages_per_pdf):
            pdfs_required=1
        else:
            pass
        pdf_number=1
        for k in range(0,total_pages,pages_per_pdf):
            pdf_writer=PdfWriter()
            pages_in_pdf=0
            while k<total_pages and pages_in_pdf<pages_per_pdf:
                page=pdf_reader.pages[k]
                pdf_writer.add_page(page)
                pages_in_pdf+=1
                k+=1
            outputpdf=(Respective_Folder)/Path(f"{base_name}-pdf-{pdf_number}.pdf")
            Respective_Folder.mkdir(parents=True, exist_ok=True)
            with open(outputpdf,"wb") as file:
                pdf_writer.write(file)
                pdf_list.append(outputpdf)
            pdf_number+=1

def pdf_to_json(pdf_input_file,Respective_Folder,no_of_papers_wanted_to_process=None):
    base_name=pdf_input_file.stem
    all_pages_reponses=""
    all_pages_reponses={"fullTextAnnotation":{"pages":[]}}
    for_pdf_comparision=re.compile(rf"{base_name}-pdf-\d+\.pdf")
    for_json_comparision=re.compile(rf"{base_name}-pdf-\d+\.json")
    pdf_splitting(pdf_input_file,Respective_Folder,no_of_papers_wanted_to_process)
    for each_pdf in Respective_Folder.iterdir():
        if re.match(for_pdf_comparision,each_pdf.name):
            pdf_to_vision_response(each_pdf,Respective_Folder)
    for each_json in Respective_Folder.iterdir():
        if re.match(for_json_comparision,each_json.name):
            all_pages_reponses=single_response_json_generation(each_json,all_pages_reponses)
if __name__=="__main__":
    pdf_input_file=Path(sys.argv[1])
    pdf_to_json(pdf_input_file,Respective_Folder=Path("OutputFolder")/pdf_input_file)