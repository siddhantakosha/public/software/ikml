from flask import Flask, request, render_template
from flask_cors import CORS
import requests
import urllib.parse
from pathlib import Path
from driver_to_call import *

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})

@app.route('/')
def index():
    return render_template('form.html')

@app.route("/library/ocr", methods=["POST"])
def download_pdf():
    print(f"Request method: {request.method}")
    print(f"Request URL: {request.url}")
    
    with open("requests.txt", "w", encoding="utf-8") as file:
        file.write(f"Method: {request.method}\n")
        file.write(f"URL: {request.url}\n")
    
    url = request.form["userInput"]
    url_split = url.split("/")
    dirp = Path("URL_based_pdfs")
    dirp.mkdir(parents=True, exist_ok=True)
    actual_filename = urllib.parse.unquote(url_split[-1])
    file_name = dirp / Path(actual_filename)
    
    if file_name.suffix == ".pdf":
        response = requests.get(url)
    else:
        return "The provided URL does not point to a PDF file."
    
    if response.status_code == 200:
        with open(file_name, 'wb') as file:
            file.write(response.content)
        
        try:
            main(file_name, None)
        except Exception as e:
            with open("Errors.txt", "w", encoding="utf-8") as error_file:
                error_file.write(str(e))
            return f"An error occurred while processing the file: {str(e)}"
        
        print(f"File saved at: {file_name}")
        return f"IKML for the file named {actual_filename} is generated!"
    else:
        return f"Failed to download PDF. Status code: {response.status_code}"

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5555)