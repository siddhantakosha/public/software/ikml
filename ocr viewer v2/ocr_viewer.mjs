/**
 * ocr_viewer.mjs
 *
 * This is a sample file for demonstrating the "drawBoundingBoxes" function for
 * drawing bounding boxes around text in a PDF. The bounding boxes are specified
 * in terms of the page's width and height, and are drawn with a red border.
 *
 * The function can be used to draw bounding boxes around text in a PDF that
 * has been OCR'd, and can be used to improve the accuracy of the OCR process.
 *
 * To use this code, you will need to have the pdfjs-dist library installed.
 * You can install it with npm by running the command "npm install --save pdfjs-dist".
 *
 * This code is released under the Apache 2.0 license. See the LICENSE file for
 * more details.
 */
import * as pdfjsLib from './pdf.mjs';

// Set the worker source
pdfjsLib.GlobalWorkerOptions.workerSrc =
'./pdf.worker.mjs';

//import * as pdfWorkerFile from './pdf.worker.mjs';
function renderPDFWithBoundingBoxes({
    pdfUrl,
    pageNum,
    pageWidth,
    pageHeight,
    zoomFactor = 1,
    arrayOfData,
    polygonSvg = "http://www.w3.org/2000/svg",
  }) {
    // HTML Elements

    const selectPages = document.getElementById("selectPage");
    const pdfContainer = document.getElementById("pdf-container");
    let pdfCanvas = document.getElementById("pdf-canvas");
    const overlay = document.getElementById("overlay");
    const textContainer = document.getElementById("text-container");

    if(pdfCanvas){
      pdfCanvas.remove();
      pdfCanvas = document.createElement("canvas");
      pdfCanvas.id = "pdf-canvas";
      pdfContainer.appendChild(pdfCanvas);
    }

    const ctx = pdfCanvas.getContext("2d");

    // Clear previous content
    overlay.innerHTML = "";
    textContainer.innerHTML = "";

    let numPages = 0;
    // Load PDF
    pdfjsLib.getDocument(pdfUrl).promise.then((pdfDoc) => {
      if(selectPages.length == 1){
        selectPages.innerHTML = Array.from({ length: pdfDoc.numPages }, (_, i) => `<option value="${i + 1}">${i + 1}</option>`).join('');
      }
      pdfDoc.getPage(pageNum).then((page) => {
        // Calculate scale based on page width and height
        const viewport = page.getViewport({ scale: (pageWidth * zoomFactor) / page.getViewport({ scale: 1 }).width });
        pdfCanvas.width = viewport.width;
        pdfCanvas.height = viewport.height;
        overlay.setAttribute("width", pdfCanvas.width);
        overlay.setAttribute("height", pdfCanvas.height);
        // Render PDF page
        const renderContext = {
          canvasContext: ctx,
          viewport: viewport,
        };
        page.render(renderContext).promise.then(() => {
          // Scale bounding box coordinates
          //console.log(arrayOfData);
          arrayOfData.forEach((box, index) => {
            const scaledPoints= box.bbox.split(";").map(pair=> {
              //console.log("index is " +index);
              const [x, y] = pair.trim().split(",").map(Number);
              return {
                x: (x+0.02) * pdfCanvas.width,
                y: (y+0.02) * pdfCanvas.height
              };
            });


            // const scaledPoints = box.bbox.split(';').map((point) => (
            //   console.log(point.x)
            // //   {
            // //   x: point.x * pdfCanvas.width,
            // //   y: point.y * pdfCanvas.height,
            // // }
            //  ));
            //console.log(box);
            // Create an SVG polygon for the bounding box
            const polygon = document.createElementNS(polygonSvg, "polygon");
            polygon.setAttribute(
              "points",
              scaledPoints.map((p) => `${p.x},${p.y}`).join(" ")
            );
            polygon.setAttribute("stroke", "red");
            polygon.setAttribute("fill", "none");
            polygon.setAttribute("stroke-width", "2");
            //overlay.appendChild(polygon);

            // Add corresponding text to the right pane
              const textArea = document.createElement("textarea");
             const textDiv = document.createElement("div");
            let string = ''
            let heightToIncrease = 0;
            box.text.forEach((text,index) => {
              string += text.content;
              // const childDiv = document.createElement("div");
              // childDiv.innerHTML = text.content;
              // childDiv.className = 'paraValue';
              // childDiv.id = text.content_id;
              // textDiv.appendChild(childDiv);
              heightToIncrease += 15;
            })
            //console.log(string+" page number is =>"+pageNum)
            textArea.value = string;
            textArea.className = 'vsValue';
            textArea.name = box.paragraph_id+"_"+box.paragraph_rel_id;
            textArea.dataset.index = index;
            textArea.style.height = `${textArea.scrollHeight+heightToIncrease}px`;
            textDiv.className = 'paragraph';
            textDiv.appendChild(textArea);
            textDiv.style.cursor = "pointer";
            //Adjust height based on content
            textArea.addEventListener('input', () => {
              textArea.style.height = 'auto'; // Reset height to calculate new height
              textArea.style.height = `${textArea.scrollHeight}px`; // Set height to scrollHeight
              overlay.appendChild(polygon);
            });

            // Highlight on hover
            textDiv.addEventListener("mouseover", () => {
              //polygon.setAttribute("stroke", "blue");
              overlay.appendChild(polygon);
            });
            textDiv.addEventListener("mouseout", () => {
              //polygon.setAttribute("stroke", "red");
              overlay.removeChild(polygon);
            });
            // textDiv.addEventListener("click", () => {
            //   //alert(`Clicked on: ${sampleText[index]}`);
            // });
            textContainer.appendChild(textDiv);
          });
        });
      });

    });

    // Re-render bounding boxes on window resize
    /*window.addEventListener("resize", () => {
      renderPDFWithBoundingBoxes({ pdfUrl, pageNum, pageWidth, pageHeight, boundingBoxes, sampleText });
    });*/
  }
  export {renderPDFWithBoundingBoxes as renderPDFWithBoundingBoxes};