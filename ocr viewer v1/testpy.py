from flask import Flask, render_template,request,jsonify
from flask import session as ses
import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from flask_cors import CORS
app = Flask(__name__)
CORS(app)
Base=declarative_base()
class Documents(Base):
    __tablename__="documents"
    title = sa.Column(sa.String)
    id = sa.Column(sa.String,primary_key=True)
    author=sa.Column(sa.String)
    pageNumbers=sa.Column(sa.Integer)
class DocPages(Base):
    __tablename__="docPages"
    doc_id=sa.Column(sa.String,sa.ForeignKey('documents.id'), nullable=False)
    page_num=sa.Column(sa.String)
    page_url=sa.Column(sa.String)
    page_content=sa.Column(sa.String)
    page_id=sa.Column(sa.String,primary_key=True)
    bbox=sa.Column(sa.String)
engine=sa.create_engine('sqlite:///C:/Users/suman/Documents/test.db')
Session=sessionmaker(bind=engine)
session=Session()
def get_all_details_documents():
    pass
def get_document_by_id(doc_id):
    document = session.query(Documents).filter_by(id=doc_id).first()
    if document:
        return document.title,document.author,document.pageNumbers
    else:
        return "Document Not Found"
def get_all_details_docPages():
    pass
def get_page_details_id_and_doc_id(page_num,doc_id):
    pagedetails=session.query(DocPages).filter(sa.and_(DocPages.doc_id==doc_id,DocPages.page_num==page_num)).first()
    return pagedetails.page_num,pagedetails.page_content,pagedetails.page_url,pagedetails.page_id,pagedetails.bbox
def get_page_url(doc_id="Rjmd",page_num=5):
    pagedetails=session.query(DocPages).filter(sa.and_(DocPages.doc_id==doc_id,DocPages.page_num==page_num)).first()
    return pagedetails.page_num,pagedetails.page_content,pagedetails.page_url,pagedetails.page_id,pagedetails.bbox
session.close()
def getting_all_data(page_num=1,doc_id="Rjmd"):
    doc_title,author_name,total_page_numbers=get_document_by_id(doc_id)
    current_pageNumber,content,image_url,page_id,bbox=get_page_details_id_and_doc_id(page_num,doc_id)
    loading_content ={
        "doc_title":doc_title,
        "author_name":author_name,
        "total_page_numbers":total_page_numbers,
        "current_pageNumber":current_pageNumber,
        "content":content,
        "bbox":bbox,
        "image_url":image_url,
        "page_id":page_id,
        "doc_id":doc_id
    }
    return (loading_content)
@app.route('/change_page', methods=['GET'])
def next_page():
    data=request.json
    doc_id=data.get("doc_id")
    page_num=data.get("currentPageNumber")
    try:
        print(data)
        current_pageNumber,content,page_url,page_id,bbox=get_page_url(doc_id,page_num)
    except Exception as e:
        print(e)
    return {
        "page_id":page_id,
        "doc_id":doc_id,
        "curr_page_num":current_pageNumber,
        "content":content,
        "bbox":bbox,
        "page_url":page_url
    }


@app.route('/get-data')
def index():
    loading_content=getting_all_data()
    return jsonify(loading_content)
if __name__ == '__main__':
    app.run(debug=True,port=8080)