class mydata
{
  static doc_id;
  static zoomLevel = 1;
  static rotation = 0;
  static viewer;
  static textZoomLevel = 1.5;
  static doc_title;
  static author_name;
  static textBox;
  static total_page_numbers;
  static current_pageNumber;
  static image_url;
  static content_with_bbox;
  static page_id;
  static img_height;
  static img_width;
  static content_all;
}

let viewer = null;
function initializeImageViewer(imageURL) {
  if (viewer)
  {
    viewer.destroy();
    viewer = null;
  }
  viewer = OpenSeadragon({
    id: 'osd-image',
    tileSources: {
      type: 'image',
      url: imageURL,
      buildPyramid: false,
    },
    showZoomControl: false,
    showHomeControl: false,
    showRotationControl: false,
    showFullPageControl: false,
    homeFillsViewer: false,
    constrainDuringPan: true,
  });
  viewer.addHandler('open', function () {
    const tiledImage = viewer.world.getItemAt(0);
    const imageWidth = tiledImage.source.dimensions.x;
    const imageHeight = tiledImage.source.dimensions.y;
    mydata.img_height=imageHeight;
    mydata.img_width=imageWidth;
    console.log(mydata.img_height);
    console.log(mydata.img_width);
  });
}
function drawRectangle(base,bbox)
{ 
  if(!bbox)
  {
    bbox=[[0,0],[0,0],[0,0],[0,0]];
  }
  console.log(bbox);
  console.log(bbox.length);
  const [[x1,y1],[x2,y2],[x3,y3],[x4,y4]]=bbox;
  var overlay = document.getElementById('overlay');
  const rect_high = Math.sqrt(((x4-x1) ** 2 + (y4 - y1) ** 2));
  const rect_width= Math.sqrt(((x2-x1) ** 2 + (y2 - y1) ** 2));
  console.log(rect_high);
  console.log(rect_width);
  if (!overlay)
  {
    overlay = document.createElement('canvas');
    overlay.id = 'overlay';
    overlay.style.position = 'absolute';
    overlay.style.left = '0';
    overlay.style.zIndex = '1';
    overlay.style.width = '100%';
    overlay.style.height = '100%';
    document.getElementById('osd-image').appendChild(overlay);
  }
  const ctx = overlay.getContext('2d');
  const containerSize = viewer.viewport.getContainerSize();
  console.log(containerSize.x);
  console.log(containerSize.y);
  overlay.width = containerSize.x;
  overlay.height = containerSize.y;
  const rect = new OpenSeadragon.Rect(x1,y1,rect_width,rect_high);
  const scaledRect = viewer.viewport.viewportToViewerElementRectangle(rect);
  ctx.clearRect(0, 0, overlay.width, overlay.height);
  if(base=="MouseIn")
  {
    ctx.strokeStyle="red";
    ctx.lineWidth = 5;
    ctx.strokeRect(scaledRect.x, scaledRect.y, scaledRect.width, scaledRect.height);
  }
  else
  {
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const textarea = document.querySelector('.textBox');
  if (textarea)
    {
      textarea.addEventListener('mousemove', (event) => {
          const lines = textarea.value.split('\n').filter(line => line.trim() !== '');
          const totalLines = lines.length;
          const lineHeight = textarea.scrollHeight / totalLines;
          const textareaTop = textarea.getBoundingClientRect().top;
          const scrollTop = textarea.scrollTop;
          const y = event.clientY - textareaTop + scrollTop;
          const lineNumber = Math.floor(y / lineHeight);
          if (lines[lineNumber]) {
              console.log(`Hovered on line ${lineNumber + 1}: ${lines[lineNumber]}`);
              mouseon(lines[lineNumber]);
          }
      });
  } else {
      console.error('Textarea element not found');
  }
});
function mouseon(text)
{
  console.log(mydata.content_with_bbox[text]);
  drawRectangle("MouseIn",mydata.content_with_bbox[text]);
}
function mouseout()
{
  drawRectangle("MouseOut")
}
function zoomIn() {
  mydata.zoomLevel += 0.2;
  updateImage();
}
function zoomOut() {
  mydata.zoomLevel = Math.max(0.2, mydata.zoomLevel - 0.2);
  updateImage();
}
function zoomNormal() {
  mydata.zoomLevel = 1;
  mydata.rotation = 0;
  updateImage();
}
function updateImage() {
  const image = document.getElementById('osd-image');
  image.style.transform = `scale(${mydata.zoomLevel}) rotate(${mydata.rotation}deg)`;
}
function rotateToLeft() {
  mydata.rotation -= 90;
  updateImage();
}
function rotateToRight() {
  mydata.rotation += 90;
  updateImage();
}
function bigText() {
  mydata.textZoomLevel += 0.2;
  updateTextZoom();
}
function smallText() {
  mydata.textZoomLevel = Math.max(0.2, mydata.textZoomLevel - 0.2);
  updateTextZoom();
}
function normalText() {
  mydata.textZoomLevel = 1.5;
  updateTextZoom();
}
function updateTextZoom() {
  mydata.textBox.style.fontSize = `${mydata.textZoomLevel}em`;
  mydata.textBox.style.transformOrigin = 'top left';
}
function changePage(direction)
{
  const page_num = document.querySelector('.current-page').textContent;
  const total_page_numbers = document.getElementById('total-pages').textContent;
  if (page_num === '1' && direction === 'prev') {
    const badpress = 'This is the first page';
    displayTemporaryMessage(badpress);
  } else if (page_num === total_page_numbers && direction === 'forward') {
    const badpress = 'This is the last Page';
    displayTemporaryMessage(badpress);
  } else {
  let pagenumber;
  if (direction === 'forward')
  {
    pagenumber = parseInt(mydata.current_pageNumber) + 1;
  }
  else
  {
    pagenumber = parseInt(mydata.current_pageNumber) - 1;
  }
  const data = {doc_id: mydata.doc_id, currentPageNumber: pagenumber};
  // console.log('called API');
  fetch('http://127.0.0.1:8080/change_page',
  {
    method: 'POST',
    headers:
    {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  })
  .then((response) => response.json())
  .then((data) =>
  {
    let mystr=data.bbox;
    let mylist=mystr.replace(",","").split(" ")
    let bbox_list=[]
    // console.log(mylist.length);
    for(let i=0;i<mylist.length;i++)
    {
      x=parseFloat(mylist[i].trim());
      y=parseFloat(mylist[i+1].trim().replace(";","").trim());
      cordinates=[x,y];
      bbox_list.push(cordinates);
      i+=1;
    }
    mydata.doc_id = data.doc_id;
    mydata.current_pageNumber = data.curr_page_num;
    mydata.image_url = data.page_url;
    mydata.page_id = data.page_id;
    mydata.content_with_bbox={[data.content]:bbox_list};
    mydata.content = data.content;
    document.querySelectorAll('.current-page').forEach((element)=>
    {
      element.textContent = mydata.current_pageNumber;
    });
    initializeImageViewer(mydata.image_url);
    const textBoxes=document.querySelectorAll('.textBox');
    console.log(textBoxes);
    textBoxes.forEach(Box=>{
      Box.textContent=Object.keys(mydata.content_with_bbox)[0];
    });
    })
    .catch((error) =>
    {
      console.error('Error:', error);
    });
  }
}
function forward()
{
  changePage('forward');
}
function prev()
{
  changePage('prev');
}
function displayTemporaryMessage(messageText)
{
  const tempMessageDiv = document.createElement('div');
  tempMessageDiv.textContent = messageText;
  tempMessageDiv.style.position = 'fixed';
  tempMessageDiv.style.bottom = '10px';
  tempMessageDiv.style.left = '50%';
  tempMessageDiv.style.transform = 'translateX(-50%)';
  tempMessageDiv.style.backgroundColor = 'rgba(0, 0, 0, 0.7)';
  tempMessageDiv.style.color = 'white';
  tempMessageDiv.style.padding = '10px 20px';
  tempMessageDiv.style.borderRadius = '5px';
  tempMessageDiv.style.zIndex = '1000';
  tempMessageDiv.style.textAlign = 'center';
  document.body.appendChild(tempMessageDiv);
  setTimeout(()=>
  {
    document.body.removeChild(tempMessageDiv);
  }, 1000);
}
function loadData()
{
  const apiUrl = 'http://127.0.0.1:8080/get-data';
  fetch(apiUrl)
    .then((response) =>
    {
      if (!response.ok)
      {
        throw new Error('Network response was not ok');
      }
      return response.json();
    })
    .then((data) =>
    {
      console.log(data);
      mydata.doc_title=data.doc_title;
      document.getElementById("doc-title").textContent=mydata.doc_title;
      mydata.author_name=data.author_name;
      document.getElementById('author-name').textContent = `written by ${mydata.author_name}`;
      mydata.current_pageNumber=data.current_pageNumber;
      curr_pag_nums=document.querySelectorAll(".current-page");
      curr_pag_nums.forEach((element)=>
      {
        element.textContent=mydata.current_pageNumber;
      });
      mydata.total_page_numbers=data.total_page_numbers;
      document.getElementById("total-pages").textContent=mydata.total_page_numbers;
      mydata.image_url=data.image_url;
      initializeImageViewer(mydata.image_url);
      mydata.content_with_bbox=data.content;
      const content_in=JSON.parse(JSON.stringify(mydata.content_with_bbox));
      console.log(content_in["184"]);
      const content=Object.keys(mydata.content_with_bbox);
      const com_con=content.join("\n");
      const txtcontent=document.querySelectorAll(".textBox");
      mydata.content_all=com_con;
      txtcontent.forEach((element)=>
      {
        element.textContent=mydata.content_all;
      }
      )
     })
    .catch((error)=>
    {
      console.error('Error loading data:', error);
    });
}
window.onload = loadData;