import fitz,os
from pathlib import Path
if __name__=="__main__":
    pdf__="C:/Users/suman/Desktop/ikml/4063_Kashmiri Devotional Poems - Pandit Nilakanth Sharma_UPSS_Sharada_0078/4063_Kashmiri Devotional Poems - Pandit Nilakanth Sharma_UPSS_Sharada_0078.pdf"
    output=Path("C:/Users/suman/Desktop/ikml/4063_Kashmiri Devotional Poems - Pandit Nilakanth Sharma_UPSS_Sharada_0078/Images")
    pdf_path=Path(pdf__)
    name=pdf__.split("/")
    name=name[-1].replace(".pdf","")
    output.mkdir(exist_ok=True,parents=True)
    pdf_doc=fitz.open(pdf_path)
    for i in range(pdf_doc.page_count):
        page = pdf_doc[i]
        zoom = 2
        mat = fitz.Matrix(zoom, zoom)
        pix = page.get_pixmap(matrix=mat)
        image_path = f'{output}/{name}-{i+1}.png'
        pix.save(image_path)