from flask import Flask,jsonify,request
from flask_cors import CORS
from pathlib import Path
from PyPDF2 import PdfReader
app=Flask(__name__)
CORS(app)
def get_content(path):
    with open(path,"r",encoding="utf-8") as fr:
        data=fr.read()
    items=data.split("\n")
    content={}
    for index in range(len(items)):
        if "[paragraph]" in items[index]:
            index+=1
            bbox=items[index].replace("[.bbox]","").replace(" ","").split(";")
            codinates=[]
            for cos in bbox:
                cos=cos.split(",")
                co_lists=[]
                for xy in cos:
                    xy=float(xy)
                    co_lists.append(xy)
                codinates.append(co_lists)
            index+=1
            vak=items[index].replace("[va]","").replace(" ","")
            content[vak]=codinates
    return content

@app.route('/change_page', methods=['POST'])
def next_page(current_pageNumber="1"):
    data=request.json
    folder_path=Path("C:/Users/suman/Desktop/ikml/4063_Kashmiri Devotional Poems - Pandit Nilakanth Sharma_UPSS_Sharada_0078")
    image_url=f'{folder_path}/{Path(folder_path.stem)}/{Path("Images")}/{folder_path.stem}{current_pageNumber}.png'
    doc_id=data.get("doc_id")
    page_num=data.get("currentPageNumber")
    content=get_content(f'{folder_path}/{Path(folder_path.stem)}-ikml-page-{current_pageNumber.zfill(3)}-ikml.txt')
    
    return{
        "page_id":"page_id",
        "doc_id":doc_id,
        "curr_page_num":current_pageNumber,
        "content":content,
        "image_url":image_url
    }
@app.route('/get-data')
def getdata():
    current_pageNumber="1"
    folder_path=Path("/4063_Kashmiri Devotional Poems - Pandit Nilakanth Sharma_UPSS_Sharada_0078")
    with open(f"{folder_path}/{Path(folder_path.stem)}.pdf","rb") as fr:
        pdf_=PdfReader(fr)
        no_pages=len(pdf_.pages)
    content=get_content(f'{folder_path}/{Path(folder_path.stem)}-ikml-page-{current_pageNumber.zfill(3)}-ikml.txt')
    image_url=f'{folder_path}/{Path("Images")}/{folder_path.stem}-{current_pageNumber}.png'
    content_loading={
        "doc_title":folder_path.stem,
        "author_name":"Un-Known",
        "total_page_numbers":no_pages,
        "current_pageNumber":current_pageNumber,
        "content":content,
        "image_url":image_url,
        "page_id":"page_id",
        "doc_id":"doc_id"
        }
    return jsonify(content_loading)

if __name__=="__main__":
    app.run(debug=True,port=8080)